FROM ubuntu:20.04
ENV TZ=America/Buenos_Aires
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update
RUN apt-get install apache2 -y tzdata
ENV APACHE_RUN_USER  www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR   /var/log/apache2
ENV APACHE_PID_FILE  /var/run/apache2/apache2.pid
ENV APACHE_RUN_DIR   /var/run/apache2
ENV APACHE_LOCK_DIR  /var/lock/apache2
ENV APACHE_LOG_DIR   /var/log/apache2
RUN mkdir -p $APACHE_RUN_DIR
RUN mkdir -p $APACHE_LOCK_DIR
RUN mkdir -p $APACHE_LOG_DIR
VOLUME var/www/html/
RUN apt update
RUN apt-get install software-properties-common -y
RUN add-apt-repository ppa:ondrej/php
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
RUN apt install php7.1 libapache2-mod-php7.1 php7.1-common php7.1-mbstring php7.1-xmlrpc php7.1-soap php7.1-apcu php7.1-smbclient php7.1-ldap php7.1-redis php7.1-gd php7.1-xml php7.1-intl php7.1-json php7.1-imagick php7.1-mysql php7.1-cli php7.1-mcrypt php7.1-ldap php7.1-zip php7.1-curl -y byobu curl git htop man unzip wget tar gzip bzip2 
RUN wget https://download.owncloud.org/community/owncloud-10.6.0.tar.bz2 && tar -jxvf owncloud-10.6.0.tar.bz2 -C /var/www/
RUN chmod 755 /var/www/owncloud
RUN chown -R www-data:www-data /var/www/owncloud
RUN ln -s /etc/apache2/sites-available/001-owncloud.conf /etc/apache2/sites-enabled/
RUN a2enmod rewrite
EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D",  "FOREGROUND"]
